# ParixisCMS

The source code for ParixisCMS is licensed under GPLv2: GNU GPL version 2 <[https://www.gnu.org/licenses/gpl-2.0.html](https://www.gnu.org/licenses/gpl-2.0.html)>

This is free software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law.

### License

ParixisCMS, a CMS for small and medium websites where contents does not change regularly. It uses SQLite.

Copyright © 2015 Bundhoo Mohammad Nadim

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2 of the license.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.