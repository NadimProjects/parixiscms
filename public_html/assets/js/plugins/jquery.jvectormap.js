/*!
 * jVectorMap version 2.0.1
 * http://jvectormap.com/
 *
 * Copyright 2011-2014, Kirill Lebedev
 *
 * Maps: http://jvectormap.com/maps/
 */
(function(a){var b={set:{colors:1,values:1,backgroundColor:1,scaleColors:1,normalizeFunction:1,focus:1},get:{selectedRegions:1,selectedMarkers:1,mapObject:1,regionName:1}};a.fn.vectorMap=function(d){var e,c,e=this.children(".jvectormap-container").data("mapObject");if(d==="addMap"){jvm.Map.maps[arguments[1]]=arguments[2]}else{if((d==="set"||d==="get")&&b[d][arguments[1]]){c=arguments[1].charAt(0).toUpperCase()+arguments[1].substr(1);return e[d+c].apply(e,Array.prototype.slice.call(arguments,2))}else{d=d||{};d.container=this;e=new jvm.Map(d)}}return this}})(jQuery);