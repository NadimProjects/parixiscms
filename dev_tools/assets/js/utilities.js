function nl2br (str, is_xhtml) {
	// Converts newlines to HTML line breaks
	//
	// version: 1109.2015
	// discuss at: http://phpjs.org/functions/nl2br
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: Philip Peterson
	// +   improved by: Onno Marsman
	// +   improved by: Atli Þór
	// +   bugfixed by: Onno Marsman
	// +      input by: Brett Zamir (http://brett-zamir.me)
	// +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: Brett Zamir (http://brett-zamir.me)
	// +   improved by: Maximusya
	// *     example 1: nl2br('Kevin\nvan\nZonneveld');
	// *     returns 1: 'Kevin\nvan\nZonneveld'
	// *     example 2: nl2br("\nOne\nTwo\n\nThree\n", false);
	// *     returns 2: '<br>\nOne<br>\nTwo<br>\n<br>\nThree<br>\n'
	// *     example 3: nl2br("\nOne\nTwo\n\nThree\n", true);
	// *     returns 3: '\nOne\nTwo\n\nThree\n'
	var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '' : '<br>';
	return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function addslashes(str) {
	//  discuss at: http://phpjs.org/functions/addslashes/
	// original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// improved by: Ates Goral (http://magnetiq.com)
	// improved by: marrtins
	// improved by: Nate
	// improved by: Onno Marsman
	// improved by: Brett Zamir (http://brett-zamir.me)
	// improved by: Oskar Larsson Högfeldt (http://oskar-lh.name/)
	//    input by: Denny Wardhana
	//   example 1: addslashes("kevin's birthday");
	//   returns 1: "kevin\\'s birthday"

	return (str + '')
		.replace(/[\\"']/g, '\\$&')
		.replace(/\u0000/g, '\\0');
}

function stripslashes(str) {
	//       discuss at: http://phpjs.org/functions/stripslashes/
	//      original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	//      improved by: Ates Goral (http://magnetiq.com)
	//      improved by: marrtins
	//      improved by: rezna
	//         fixed by: Mick@el
	//      bugfixed by: Onno Marsman
	//      bugfixed by: Brett Zamir (http://brett-zamir.me)
	//         input by: Rick Waldron
	//         input by: Brant Messenger (http://www.brantmessenger.com/)
	// reimplemented by: Brett Zamir (http://brett-zamir.me)
	//        example 1: stripslashes('Kevin\'s code');
	//        returns 1: "Kevin's code"
	//        example 2: stripslashes('Kevin\\\'s code');
	//        returns 2: "Kevin\'s code"

	return (str + '')
		.replace(/\\(.?)/g, function(s, n1) {
			switch (n1) {
				case '\\':
					return '\\';
				case '0':
					return '\u0000';
				case '':
					return '';
				default:
					return n1;
			}
		});
}

function check_email(email) {

	email_length = email.length;
	if (email_length < 8) return false;
	else if( /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/.test( email )) return true;
	else return false;
}

function fixedEncodeURIComponent(str) {
	return encodeURIComponent(str)
		.replace(/\*/g,	'%2A')
		.replace(/\+/g,	'%2B')
		.replace(/!/g,	'%21')
		.replace(/'/g,	'%27')
		.replace(/\(/g,	'%28')
		.replace(/\)/g,	'%29');
}