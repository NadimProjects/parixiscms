# [ParixisCMS](https://bitbucket.org/NadimProjects/parixiscms)

This is a CMS that uses SQLite as DB. It can be used for small and medium 
websites where contents does not change regularly. It is powerful enough to 
be multi-site and multi-language. Developed using FatFreeFramework, Smarty 
Templates & Twitter's Bootstrap.

## Installation

Create the VHosts. (see vhost_My_Project.conf for an example). You'll need 
to update **webapp/config/params.php** accordingly.

*It should run right away*

## Demo

[http://parixiscms.webmauritius.net/](http://parixiscms.webmauritius.net/)

## Demo Admin

[http://parixiscms.webmauritius.net/admin](http://parixiscms.webmauritius.net/admin)

* User = root
* Pass = nadim

You can change the admin entry point (see webapp/config/params.php).

## SQLite
All SQLite DB files are stored in webapp/DB folder. Make sure the folder and 
each .sqlite file have read/write access.

## Sessions and other files
Sessions files and other compiled files (e.g. Smarty) are stored in **storage** 
folder. Please make sure the sub-folders are writeable.

## Customisation

You can define your host(s) in **webapp/config/params.php**.

For each host, if you have separate templates (folders), then you have to create 
them in **webapp/view**. The format of the folder is: 'host_' + template_folder you 
defined for your host (in webapp/config/params.php). You need a **main_controller.tpl** 
inside each of the folders.
