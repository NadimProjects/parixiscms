<?php

namespace Model;

class Users extends \Plugins {

	private $user = array(
		'user_id'	=> '',
		'login'		=> '',
		'passwd'	=> '',
		'username'	=> '',
		'gender'	=> '',
		'position'	=> ''
	);

	function load_by_id($id = '') {

		if (is_numeric($id) && $id > 0) {

			$mapper	= new \DB\SQL\Mapper($this->DB, 'users');
			$mapper->load('user_id = ' . $id);

			if ($mapper->dry()) {
				$this->error = 'User not found';
				return FALSE;
			}

			// We have data for the page from the DB
			foreach(array_keys($this->user) as $k) {
				$this->user[$k] = $mapper->get($k);
			}

			$this->message = 'User found';
			return TRUE;
		}

		$this->error = 'User: ID was not provided';
		return FALSE;
	}

	function load_by_values($values = array()) {

		if (!empty($values) && is_array($values)) {

			$at_least_one_value_stored = FALSE;

			foreach(array_keys($values) as $k) {

				if (isset($this->user[$k])) {
					$at_least_one_value_stored = TRUE;
					$this->user[$k] = $values[$k];
				}
			}

			if ($at_least_one_value_stored) {

				$this->message = 'User: value(s) has/have been stored';
				return TRUE;
			}
			else {

				$this->error = 'User: key(s) provided in key-value pair(s) not found';
				return FALSE;
			}
		}

		$this->error = 'User: value(s) was/were not provided';
		return FALSE;
	}

	function set_value($key = '', $value = '') {

		return $this->load_by_values(array($key => $value));
	}

	function get_value($key = '') {

		if (isset($this->user[$key]))
			return $this->user[$key];
		else {
			$this->error = 'Value not found';
			return FALSE;
		}
	}

	function get_user() {
		return $this->user;
	}

	function get_all_users($filter = '') {

		// List of users
		$users = array();

		$mapper	= new \DB\SQL\Mapper($this->DB, 'users');
		$mapper->load($filter);

		while(!$mapper->dry()) {

			$mapper->copyto('foobar');
			$users[] = \Base::instance()->get('foobar');
			$mapper->next();
		}

		return $users;
	}

	function save_user() {

		// If not in admin, then OUT !
		if (!$this->is_admin) {

			$this->error = 'Not allowed!';
			return FALSE;
		}

		$errors = array();

		// If updating, then login won't be changed and password is not required (may keep old password)
		// If new user, then both login and password are mandatory

		// No 'user_id' -> insert (new user) - If 'user_id' -> update (existing)
		if (empty($this->user['user_id'])) {

			if (empty($this->user['login']))
				$errors[] = 'Login is mandatory';

			if (empty($this->user['passwd']))
				$errors[] = 'Password is mandatory';
		}

		if (empty($this->user['username']))
			$errors[] = 'Name of user is mandatory';

		// Errors !!!
		if (!empty($errors))  {

			$this->error = 'Cannot save user. Reason(s): ' . implode('. ', $errors);
			return FALSE;
		}

		// User (Active Record Cursor class)
		$mapper	= new \DB\SQL\Mapper($this->DB, 'users');

		// Make sure that login is not already present (unless updating self)
		if (empty($this->user['user_id'])) {

			$mapper->load(array(
				'login = :login',
				':login' => $this->user['login'],
			));

			// We've found one User with same login
			if (!$mapper->dry()) {
				$this->error = 'LOGIN already exists.';
				return FALSE;
			}
		}

		// No errors. Put default values if values not present
		if (empty($this->page['gender']))
			$this->page['gender']	= 'M';		// Male by default? Should we put 'F'? Dunno...

		// If existing, load it, update the values and save
		if (!empty($this->user['user_id'])) {

			$mapper->load(array(
				'user_id = :user_id',
				':user_id' => $this->user['user_id'],
			));

			// We do not have a user with this 'user_id' ! How to update?
			if ($mapper->dry()) {
				$this->error = 'Cannot update. User not found.';
				return FALSE;
			}

			// If password is provided: update, else keep old password
			if (!empty($this->user['passwd'])) {

				$Password			= new \Model\Password();
				$mangledPassword	= $Password->GetPassword($this->user['passwd']) ;
				$mapper->set('passwd',	$mangledPassword);

				// Unset password ...
				$this->user['passwd'] = '';
			}
		}
		// New user
		else {

			$Password	= new \Model\Password();

			$mapper->set('user_id',	''); // Ensure '' (because if zero, it counts at empty() also; and with 0, no auto-incremented 'user_id' will be generated)
			$mapper->set('login',	$this->user['login']);
			$mapper->set('passwd',	$Password->GetPassword($this->user['passwd']));
		}

		// Map other values
		$mapper->set('username',	$this->user['username']);
		$mapper->set('gender',		$this->user['gender']);
		$mapper->set('position',	$this->user['position']);

		try {

			// Save
			$mapper->save();

			if (empty($this->user['user_id']))
				$this->user['user_id'] = $mapper->get('_id');

			$this->message = 'User has been saved';

			return $this->user['user_id'];
		}
		catch(\Exception $e) {

			$this->error = $e->getMessage();
			return FALSE;
		}
	}
}
