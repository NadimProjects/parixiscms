<?php

class Plugins {

	// Instance of $F3 to be used by plugins (if instantiated in Controller class) ...
	protected $F3			= NULL;
	protected $DB			= NULL;
	protected $is_admin		= FALSE;

	protected $message		= '';
	protected $error		= '';

	function __construct() {

		if (\Registry::exists('F3'))
			$this->F3		= \Registry::get('F3');

		if (\Registry::exists('DB'))
			$this->DB		= \Registry::get('DB');

		if (\Registry::exists('is_admin')) {
			$is_admin		= \Registry::get('is_admin');

			if (empty($is_admin->scalar))
				$this->is_admin = FALSE;
			else
				$this->is_admin	= $is_admin->scalar;
		}
	}

	function get_last_message() {
		return $this->message;
	}

	function get_last_error() {
		return $this->error;
	}
}