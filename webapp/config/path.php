<?php

if (!defined('APP_started')) { header('Location: /'); exit(0); }

define('path_Config',			__DIR__);
define('path_WEB_APP',			dirname(path_Config));
define('path_DocumentRoot',		dirname(path_WEB_APP)	. '/public_html');
define('path_Storage',			dirname(path_WEB_APP)	. '/storage');

define('path_Plugins',			path_WEB_APP			. '/plugins');
define('path_View',				path_WEB_APP			. '/view');
define('path_Controller',		path_WEB_APP			. '/controller');

define('path_LIBS',				path_WEB_APP			. '/lib');
define('path_F3',				path_LIBS				. '/F3-v3.4');

define('path_Smarty',			path_LIBS				. '/Smarty-3.1.21/libs');
define('path_Smarty_cache',		path_Storage			. '/Smarty/cache');			// chmod 777
define('path_Smarty_compiled',	path_Storage			. '/Smarty/compiled');		// chmod 777
define('SMARTY_DIR',			path_Smarty				. DIRECTORY_SEPARATOR);

define('path_Sessions',			path_Storage			. '/sessions');				// chmod 777

define('path_SQLiteDB',			path_WEB_APP			. '/DB');

define('path_extMarkdown',		path_WEB_APP			. '/markdown');
define('path_extScripts',		path_WEB_APP			. '/scripts');