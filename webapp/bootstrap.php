<?php

// The web app starts
define('APP_started'		, TRUE);

include __DIR__				. '/config/path.php';

// Utility functions
include path_LIBS			. '/utilities.php';

// Set some params
include path_Config			. '/params.php';

// Smarty templates
include path_Smarty			. '/Smarty.class.php';

// Start FatFreeFramework
$F3 = require(path_F3		. '/base.php');

// We are working with which host ?
if (isset($app_data[ $F3->get('HOST') ])) {

	// Path where to save sessions
	session_save_path(path_Sessions);
	// See:			https://www.appnovation.com/blog/session-garbage-collection-php
	// More info:	http://stackoverflow.com/questions/520237/how-do-i-expire-a-php-session-after-30-minutes/1270960#1270960
	ini_set('session.gc_probability', 1); // Garbage collection in Debian / Ubuntu + reduce risk of session hijacking

	// URI for backend (admin interface)
	define('DYN_ADMIN',		$app_data[ $F3->get('HOST') ]['admin_entry_point']);

	// Application data
	$F3->set('APP_DATA',	$app_data[ $F3->get('HOST') ]);

	// Time started at
	$F3->set('start_time',	$start_time);

	// UI folder (usually for fonts only, since we are using Smarty templates)
	$F3->set('UI',			path_View . DS . 'host_' . $F3->get('APP_DATA.template_folder'));

	// Default timezone
	$F3->set('TZ',			$F3->get('APP_DATA.timezone'));

	// Default language & available languages
	$F3->set('DEFAULT_LANG',$F3->get('APP_DATA.default_lang'));
	$F3->set('AVAIL_LANGS',	$F3->get('APP_DATA.languages'));

	// Autoload classes (controllers & non-F3 plugins / models)
	$F3->set('AUTOLOAD',	path_Controller.'/;' . path_Plugins.'/');

	// Specifics to DEV & PROD environments
	if ($F3->get('APP_DATA.project_mode') == 'DEV') {

		// Debug level
		$F3->set('DEBUG',	3);

		// If we are in development environment, we provide all defined hosts
		$F3->set('HOSTS',		array_keys($app_data));
	}
	else {

		// Debug level
		$F3->set('DEBUG',	0);

		// On production environment, we provide only PROD hosts
		$hosts = array();
		foreach($app_data as $host => $v) {
			if ($v['project_mode'] == 'PROD')
				$hosts[]	= $host;
		}

		$F3->set('HOSTS',	$hosts);
	}

	// Routes (hence URIs) are case sensitive
	$F3->set('CASELESS',	FALSE);

	// Define routes
	include path_Config		. '/routes.php';

	$F3->set('T_LOG',		'Y-m-d H:i:s');
	$F3->set('LOGS',		__DIR__ . '/logs/');

	// Run
	$F3->run();
}
else {

	$F3->error(500);
}
