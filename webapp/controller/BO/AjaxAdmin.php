<?php

namespace BO;

class AjaxAdmin extends \Admin {

	function beforeRoute($F3) {

		if (!$F3->get('AJAX')) {
			$F3->reroute(DYN_ADMIN);
			exit(0);
		}

		parent::beforeRoute($F3);
	}

	function dispatcher($F3, $args) {

		$request = explode('/', $args['request']);

		if (method_exists($this, $request[0]))
			$this->$request[0]($F3, $args, $request);					// Call a method in this class
		elseif(file_exists(__DIR__ . '/AJAX/' . $request[0] . '.php'))
			include(__DIR__ . '/AJAX/' . $request[0] . '.php');			// Include a file
	}
}
