<?php

if (!$F3->devoid('POST.col') && !$F3->devoid('POST.key_vals')) {

	$column = '';
	switch ($F3->get('POST.col')) {
		case 'A': $column = 'rank';		break;
		case 'B': $column = 'active';	break;
	}

	if ($column != '') {

		$sql = "UPDATE menus SET {$column} = :col_val WHERE menu_id = :menu_id";

		$sql_list = array();
		$val_list = array();

		$key_vals = explode('#', $F3->get('POST.key_vals'));

		if (!empty($key_vals)) {

			foreach($key_vals as $kv) {

				list($menu_id, $col_val) = explode('|', $kv);

				$sql_list[] = $sql;
				$val_list[] = array(':col_val' => $col_val, ':menu_id' => $menu_id);
			}

			try {
				$result = $this->DB->exec($sql_list, $val_list);

				$this->AJAX_resp['status']	= 'OK';
				$this->AJAX_resp['msg']		= 'Successfully saved';
			}
			catch (Exception $e) {
				$this->AJAX_resp['msg']		= $e->getMessage();
			}
		}
		else {
			$this->AJAX_resp['msg'] = 'No values were provided (to save)';
		}
	}
	else {
		$this->AJAX_resp['msg'] = 'Column (to update) was not provided';
	}
}
