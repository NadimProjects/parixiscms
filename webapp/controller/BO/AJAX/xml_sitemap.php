<?php

$xml_filename = Web::instance()->slug($F3->get('HOST'));

if (is_writeable(path_DocumentRoot.'/'.$xml_filename.'.xml') && is_writeable(path_DocumentRoot.'/'.$xml_filename.'-index.xml')) {

	// https://github.com/o/sitemap-php
	$sitemap = new \Sitemap( 'http://'.$F3->get('HOST') );

	$sitemap->setPath(path_DocumentRoot . '/');
	$sitemap->setFilename($xml_filename);

	// CMS instance
	$CMS = new \Model\CMS();

	// Change frequency, verbose
	$change_frequency = $CMS->get_change_frequency_list();

	// Get all pages
	$pages = $CMS->get_all_pages('active = "Y"');
	foreach($pages as $k => $v) {

		// Change frequency
		$v['change_freq'] = $change_frequency[ $v['change_freq'] ];

		// SEO: priority
		if ($v['priority'] == 'A')
			$v['priority'] = '1.0';
		else
			$v['priority'] = $v['priority'] / 10;

		$sitemap->addItem($v['url'], $v['priority'], $v['change_freq'], date('Y-m-d', $v['last_updated']));
	}

	$sitemap->createSitemapIndex('http://'.$F3->get('HOST').'/', 'Today');

	$this->AJAX_resp['status']		= 'OK';
	$this->AJAX_resp['msg']			= 'Successfully created XML sitemap';
	$this->AJAX_resp['sitemap']		= $xml_filename;
	$this->AJAX_resp['xml_sitemap']	= implode('', file(path_DocumentRoot.'/'.$xml_filename.'.xml'));
}
else {

	$this->AJAX_resp['msg']			= '<b>XML sitemap</b>: XML files are not writeable or not present. Please create "<b>' . $xml_filename . '.xml</b>" and "<b>' . $xml_filename . '-index.xml</b>" if not created yet (in public html folder). Give write permission on the files.';
}
