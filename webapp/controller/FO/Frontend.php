<?php

namespace FO;

class Frontend extends \Controller {

	function show($F3, $args) {

		$page_found = FALSE;
		$URI_parts	= parse_url($F3->get('URI'));

		// Instance of CMS
		$CMS = new \Model\CMS();

		// Menus -------------------------------------------------------------------------------------------------------
		$menus = $CMS->get_menus_with_links();
		$this->smarty->assign('menus', $menus);

		// Do you need a breadcrumb? -----------------------------------------------------------------------------------
		$this->breadcrumb = $CMS->get_breadcrumb($URI_parts['path']);

		// Pages -------------------------------------------------------------------------------------------------------
		$CMS->load_page_by_url($URI_parts['path']);
		$page = $CMS->get_page();

		if ($page) {

			$page_found = TRUE;
			$page['other_contents'] = '';

			// Markdown
			if ($page['content_type'] == 'M') {
				$page['content'] = \Markdown::instance()->convert($page['content']);
			}

			// External files / script?
			if (in_array($page['content_type'], array('S', 'T')) && !empty($page['external_file'])) {

				$file = basename($page['external_file']);

				if (substr($page['external_file'], 0, 5) == 'MD://') {

					if (file_exists(path_extMarkdown . '/' . $file)) {

						$markdown = file_get_contents(path_extMarkdown . '/' . $file);
						$page['other_contents'] = \Markdown::instance()->convert($markdown);
					}
				}
				elseif (substr($page['external_file'], 0, 6) == 'PHP://') {

					if (file_exists(path_extScripts . '/' . $file)) {

						include(path_extScripts . '/' . $file);
					}
				}
			}

			// ROBOTS: index ?
			if ($page['indexme'] == 'Y')
				$page['indexme'] = 'INDEX';
			else
				$page['indexme'] = 'NOINDEX';

			// ROBOTS: follow ?
			if ($page['followme'] == 'Y')
				$page['followme'] = 'FOLLOW';
			else
				$page['followme'] = 'NOFOLLOW';
		}

		// Page details
		$this->smarty->assign('page',		$page);
		$this->smarty->assign('page_found',	$page_found); // $F3->error(404); ?
		$this->smarty->assign('URI_path',	$URI_parts['path']);
	}
}