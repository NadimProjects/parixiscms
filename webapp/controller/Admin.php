<?php

class Admin extends Controller {

	// Amount of time in seconds, after which, if user remained inactive, user shall be logged out
	private $inactivity_timeout = 900; // 900 seconds = 15 minutes

	// HTTP route pre-processor
	function beforeRoute($F3) {

		// Not already authenticated ?
		if ($F3->devoid('SESSION.AdminAuth.user_id')) {
			$F3->reroute(DYN_ADMIN . '/login');
			exit(0);
		}

		// Procedural style (Object oriented style not working)
		$now	= date_create();
		$ts		= date_timestamp_get($now);

		// Log user out if inactive after $inactivity_timeout (On Prod servers only! Continue on dev servers)
		if ($F3->get('APP_DATA.project_mode') == 'PROD' && ($ts - $F3->get('SESSION.AdminAuth.last_ts')) > $this->inactivity_timeout) {
			$F3->reroute(DYN_ADMIN . '/logout');
			exit(0);
		}

		// Update activity timestamp
		$F3->set('SESSION.AdminAuth.last_ts', $ts);

		parent::beforeRoute($F3);

		// This is admin
		$this->is_admin	= TRUE;

		$is_admin = (object) $this->is_admin;
		\Registry::set('is_admin',	$is_admin);

		// Only needed if not AJAX
		if (!$F3->get('AJAX')) {

			// Content title
			$this->content_title	= 'Dashboard';

			// Breadcrumb
			$this->breadcrumb[] = array(
				'active'	=> FALSE,
				'href'		=> DYN_ADMIN,
				'display'	=> '<i class="fa fa-dashboard"></i> Dashboard'
			);

			$this->smarty->assign('AdminAuth',	$F3->get('SESSION.AdminAuth'));
		}
	}
}
