{*
	{include file="D3_vertical_tree.tpl" tree_id="tree_id" tree_data=$tree_data tree_width=W tree_height=H click_callback="js_callback_function(this)"}

	$tree_arr[] = array(
		'name'		=> ROOT,
		'info'		=> array(any structure that will be retrieved thr' HTML5 data property in js_callback_function() for example),
		'children'	=> array($tree_arr, $tree_arr, ...) (recursive)
	);
	$tree_data = json_encode($tree_arr);

	Note:	1. $tree_arr[]['info'] will be JSON.stringified (JSON.stringify()) then addslashed. When retrieving, slashes should
			be stripped first, then JSON.parse() - var info = JSON.parse(stripslashes( $(this).data('info') ));
			2. addslashes() and stripslashes(): see /js/common/functions.js

	// Example of $tree_data & $tree_arr
	// ---------------------------------
	$tree_data = json_encode(array(
		array(
			'name' => 'ROOT',
			'info' => array(
				'id' => 999,
				'foo' => 'bar'
			),
			'children' => array(
				array(
					'name' => 'ROOT',
					'info' => array(
						'id' => 999,
						'foo' => 'bar'
					)
				),
				array(
					'name' => 'ROOT',
					'info' => array(
						'id' => 999,
						'foo' => 'bar'
					),
					'children' => array(
						array(
							'name' => 'ROOT',
							'info' => array(
								'id' => 999,
								'foo' => 'bar'
							)
						)
					)
				)
			)
		)
	));
*}

{if isset($tree_id) && isset($tree_data)}

	{if !isset($D3_generate_vertical_tree)}
		{assign var="D3_generate_vertical_tree" value="1" scope="global"}

		{strip}<style type="text/css">
			.node {
				cursor: pointer;
			}
			.node circle {
				fill: #fff;
				stroke: #367fa9;
				stroke-width: 1px;
			}
			.node text { font: 12px "Courier New", Monospace; }
			.link {
				fill: none;
				stroke: #ccc;
				stroke-width: 2px;
			}
			.node:hover circle {
				fill: #367fa9;
				stroke: #003;
			}
			.node:hover text {
				fill: #367fa9;
				font-weight: bold;
			}
		</style>{/strip}

		{capture name="javascript"}{strip}
			<script src="/js/d3.min.js"></script>
			<script type="text/javascript">
				function D3_generate_vertical_tree(tree_id, data, w, h, click_callback, draggable, node_separation) {

					if (typeof draggable == 'undefined')		draggable		= false;
					if (typeof node_separation == 'undefined')	node_separation	= 3;

					var margin		= { top: 40, right: 40, bottom: 40, left: 40 },
						width		= w - margin.right - margin.left,
						height		= h - margin.top - margin.bottom;
					var tree		= d3.layout.tree().size([height, width]);
					var diagonal	= d3.svg.diagonal().projection(function(d) {
						var x		= d.x * node_separation;
						return [x, d.y];
					});
					var svg			= d3.select('#' + tree_id)
						.append('svg')
						.attr('id', 'svg_' + tree_id)
						.attr('style', 'cursor:move')
						.attr('width', width + margin.right + margin.left)
						.attr('height', height + margin.top + margin.bottom)
						.append('g')
						.attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

					{* jQuery UI draggable MUST be loaded separately *}
					if (window.jQuery.ui && draggable) $('#svg_' + tree_id).draggable();

					{* Compute the new tree layout *}
					var nodes = tree.nodes(data),
						links = tree.links(nodes);

					{* Normalize for fixed-depth *}
					nodes.forEach(function(d) { d.y = d.depth * 50; });

					{* Declare the nodes *}
					var i = 0;
					var node = svg.selectAll('g.node')
						.data(nodes, function(d) { return d.id || (d.id = ++i); });

					{* Enter the nodes *}
					var nodeEnter = node.enter()
						.append('g')
						.attr('class', 'node')
						.attr('data-name', function(d) { return addslashes(d.name); })
						.attr('data-info', function(d) {
							if  (typeof d.info != undefined) {
								return addslashes(JSON.stringify( d.info ));
							}
							else {
								return false;
							}
						})
						.attr('onclick', click_callback)
						.attr('transform', function(d) {
							var x = d.x * node_separation;
							return 'translate(' + x + ',' + d.y + ')';
						});

					nodeEnter.append('circle').attr('r', 6);
					nodeEnter
						.append('text')
						.attr('lengthAdjust', 'spacingAndGlyphs')
						.attr('onclick', click_callback)
						.attr('y', function(d) { return d.children || d._children ? -15 : 15; })
						.attr('dy', '.35em')
						.attr('text-anchor', 'middle')
						.text(function(d) { return d.name; })
						.style('fill-opacity', 1);

					{* Declare the links *}
					var link = svg.selectAll('path.link').data(links, function(d) { return d.target.id; });

					{* Enter the links *}
					link.enter().insert('path', 'g')
						.attr('class', 'link')
						.attr('d', diagonal);
				}
			</script>
		{/strip}{/capture}
		{append var='deffered_javascript_code' value=$smarty.capture.javascript scope='global'}
	{/if}

	{if !isset($tree_width)}{assign var="tree_width" value="2000"}{/if}
	{if !isset($tree_height)}{assign var="tree_height" value="500"}{/if}
	{if !isset($click_callback)}{assign var="click_callback" value=";"}{/if}
	{if !isset($draggable)}{assign var="draggable" value="0"}{/if}
	{if !isset($node_separation)}{assign var="node_separation" value="3"}{/if}

	<div id="{$tree_id}" style="width:100%;height:{$tree_height}px;cursor:hand;overflow:hidden"></div>
	{capture name="javascript"}{strip}
		<script type="text/javascript">
			$('document').ready(function() {
				var data = {$tree_data}; {* $data = [{ ... }]; // JSON data *}
				D3_generate_vertical_tree('{$tree_id}', data[0], {$tree_width}, {$tree_height}, '{$click_callback}', {$draggable}, {$node_separation});
			});
		</script>
	{/strip}{/capture}
	{append var='deffered_javascript_code' value=$smarty.capture.javascript scope='global'}
	{* Reset eveything, except $D3_generate_vertical_tree. Need to redefine these vars if calling again *}
	{assign var="tree_id" value=""}
	{assign var="tree_data" value=""}
	{assign var="tree_width" value=""}
	{assign var="tree_height" value=""}
	{assign var="click_callback" value=""}
{/if}
