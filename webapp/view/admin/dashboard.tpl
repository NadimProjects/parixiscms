<h3>Welcome !</h3>
<p>Welcome to <b>{$APP_DATA.app_name|default:'Parixis CMS'}</b>! You'll be able to manage the contents of your website from this BackOffice.</p>

<h3>XML Sitemap</h3>
<p>
	Please do not forget to generate your XML sitemap after adding, modifing or deleting (contents of) pages.<br /><br />
	<a id="update_btn" href="javascript:generate_xml_sitemap()" class="btn btn-mini btn-success">Update my XML sitemap</a>
	<img id="loader" src="/images/AdminLTE/ajax-loading.gif" style="width:35px;display:none" />
</p>
<div style="display:none" id="sitemap_container">
	<h4><a id="xml_url" href="" target="_blank">XML Sitemap <i class="fa fa-external-link"></i></a> was generated. You may submit <a id="xml_index" href="" target="_blank">this file <i class="fa fa-external-link"></i></a> to search engines.</h4>
	<pre style="font-size:11px" id="XML_sitemap"></pre>
</div>
<div class="alert alert-danger" style="display:none" role="alert" id="alert"> </div>

{capture name="JSSnippet"}{strip}
<script type="text/javascript">

	function generate_xml_sitemap() {

		$('#sitemap_container').hide();
		$('#alert').hide();
		$('#loader').show();

		$.ajax({
			url			: '{$smarty.const.DYN_ADMIN}/ajax/xml_sitemap',
			type		: 'POST',
			dataType	: 'json',
			data		: ''
		})
		.done(function(res) {

			if (res.status == 'OK') {

				$('#xml_url').prop('href', 'http://{$HOST}/' + res.sitemap + '.xml');
				$('#xml_index').prop('href', 'http://{$HOST}/' + res.sitemap + '-index.xml');
				$('#XML_sitemap').text(res.xml_sitemap);
				$('#sitemap_container').show();
				var pos = $('#update_btn').position();
				window.scroll(0, pos.top);
			}
			else {
				$('#alert').html(res.msg).show();
			}

			$('#loader').hide();
		})
		.fail(function(jqXHR, textStatus, errorThrown) { $('#loader').show(); });
	}

</script>
{/strip}{/capture}
{append var='PostponedJS' value=$smarty.capture.JSSnippet scope='global'}
