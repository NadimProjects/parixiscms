<style>
	.c { text-align: center; }
	.w75 { width: 75px; }
</style>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="box box-solid box-primary">
			<div class="box-header"><h3 class="box-title">List of users</h3></div>
			<div class="box-body">
				<table class="table table-bordered table-condensed table-hover">
					<thead>
					<tr style="white-space:nowrap">
						<th class="c w75">#</th>
						<th>Name</th>
						<th>Login</th>
						<th>Gender</th>
						<th>Position</th>
						<th class="w75"></th>
					</tr>
					</thead>
					<tbody id="List_of_Menus">
					{assign var="idx" value="0"}
					{foreach $users as $v}
						{math equation="x + 1" x=$idx assign="idx"}
						<tr>
							<td class="c">{$idx}</td>
							<td>{$v.username}</td>
							<td>{$v.login}</td>
							<td>{if $v.gender eq 'F'}Female{else}Male{/if}</td>
							<td>{$v.position}</td>
							<td class="c"><a target="_blank" href="{$smarty.const.DYN_ADMIN}/users_management/view/{$v.user_id}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit</a></td>
						</tr>
					{foreachelse}
					<tr><td colspan="99" class="c">There are no users for the moment. <a class="btn btn-success btn-xs" href="{$smarty.const.DYN_ADMIN}/users_management/add"><i class="fa fa-plus"></i> Add a user</a></td></tr>
					{/foreach}
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
