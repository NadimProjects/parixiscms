<div class="box box-primary">
	<div class="box-header"><h3 class="box-title"></h3></div>
	<form class="form-horizontal clearfix" method="POST" id="frm" action="{$smarty.const.DYN_ADMIN}/users_management/save">
		<input type="hidden" name="fld[user_id]" value="{$user.user_id}" />
		<input type="hidden" name="profile" value="{$profile}" />
		<div class="form-group" id="fg_login">
			<label class="col-sm-2 control-label" for="fld_login">Login</label>
			<div class="col-sm-2"><input value="{$user.login}" class="form-control" type="text" id="fld_login" name="fld[login]" placeholder="Login"{if !empty($user.user_id)} readonly="readonly"{/if} /></div>
		</div>
		<div class="form-group" id="fg_passwd">
			<label class="col-sm-2 control-label" for="fld_passwd">Password</label>
			<div class="col-sm-2"><input value="" class="form-control" type="password" id="fld_passwd" name="fld[passwd]" placeholder="Password" /></div>
		</div>
		<div class="form-group" id="fg_username">
			<label class="col-sm-2 control-label" for="fld_username">Your name</label>
			<div class="col-sm-3"><input value="{$user.username}" class="form-control" type="text" id="fld_username" name="fld[username]" placeholder="Your name" /></div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Gender</label>
			<div class="col-sm-10">
				<input type="radio" name="fld[gender]" value="M"{if $user.gender neq 'F'} checked="checked"{/if} /> Male
				<input type="radio" name="fld[gender]" value="F"{if $user.gender eq 'F'} checked="checked"{/if} /> Female
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="fld_position">Position</label>
			<div class="col-sm-3"><input value="{$user.position}" class="form-control" type="text" id="fld_position" name="fld[position]" placeholder="Position, e.g. Admin, Moderator" /></div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button id="submit" class="btn btn-primary" style="float:left;">Save</button>
				<div id="alert" class="alert alert-danger" role="alert" style="display:none;float:left;padding:5px;margin-bottom:0">Please fill the required fields (highlighted in red).</div>
			</div>
		</div>
	</form>
</div>
{capture name="JSSnippet"}{strip}
	<script type="text/javascript">
		$(document).ready(function() {
			$('#submit').on('click', function(e) {

				$('#alert').hide();
				$('#fg_login').removeClass('has-error');
				$('#fg_passwd').removeClass('has-error');
				$('#fg_username').removeClass('has-error');

				var mandatory_empty = false;

				{if empty($user.user_id)}

					if ($.trim($('#fld_login').val()) == '') {
						e.preventDefault();
						$('#fg_login').addClass('has-error');
						var mandatory_empty = true;
					}

					if ($.trim($('#fld_passwd').val()) == '') {
						e.preventDefault();
						$('#fg_passwd').addClass('has-error');
						var mandatory_empty = true;
					}

				{/if}

				if ($.trim($('#fld_username').val()) == '') {
					e.preventDefault();
					$('#fg_username').addClass('has-error');
					var mandatory_empty = true;
				}

				if (mandatory_empty) {
					$('#alert').show();
					return;
				}
			});
		});
	</script>
{/strip}{/capture}
{append var='PostponedJS' value=$smarty.capture.JSSnippet scope='global'}
