{assign var="_folder_" value="admin/admin_config/users"}

{if $pg_type eq 'UsersList'}
	{include file="`$_folder_`/users_list.tpl"}
{elseif $pg_type eq 'UsersAdd' or $pg_type eq 'UsersView'}
	{include file="`$_folder_`/users_form.tpl"}
{/if}

{assign var="_folder_" value=""}