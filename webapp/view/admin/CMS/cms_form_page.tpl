<link rel="stylesheet" type="text/css" href="/assets/css/plugins/codemirror/codemirror.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/plugins/codemirror/theme/blackboard.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/plugins/codemirror/theme/monokai.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/plugins/summernote/summernote.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/plugins/ion.rangeSlider.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/plugins/ion.rangeSlider.skinNice.css" />
<style type="text/css">
	.CodeMirror { font-size:11px; border-top: 1px solid black; border-bottom: 1px solid black; }
	.cm-s-default .cm-trailing-space-a:before,
	.cm-s-default .cm-trailing-space-b:before { position: absolute; content: "\00B7"; color: #777; }
	.cm-s-default .cm-trailing-space-new-line:before { position: absolute; content: "\21B5"; color: #777; }
</style>
<div class="box box-primary">
	<div class="box-header"><h3 class="box-title"></h3></div>
	<form class="form-horizontal clearfix" method="POST" id="frm" action="{$smarty.const.DYN_ADMIN}/CMS/pages/save">
		<input type="hidden" name="fld[page_id]" value="{$page.page_id}" />
		<div class="form-group" id="fg_title">
			<label class="col-sm-2 control-label" for="fld_title">Title</label>
			<div class="col-sm-6"><input value="{$page.title}" class="form-control" type="text" id="fld_title" name="fld[title]" placeholder="Title" /></div>
		</div>
		<div class="form-group" id="fg_url">
			<label class="col-sm-2 control-label" for="fld_url">URL of page</label>
			<div class="col-sm-6">
				<div class="input-group">
					<input value="{$page.url}" class="form-control" type="text" id="fld_url" name="fld[url]" placeholder="URL of page" />
					<a href="javascript:generate_url()" title="Generate a URL" class="input-group-addon"><i class="fa fa-magic"></i></a>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="fld_content_type">Content type</label>
			<div class="col-sm-6">{include file="widgets/select_box.tpl" _sel_id="fld_content_type" _sel_name="fld[content_type]" _sel_option=$content_type _selected_val=$page.content_type}</div>
		</div>
		<div class="form-group" id="fg_ext_file" style="display:none">
			<label class="col-sm-2 control-label" for="fld_external_file">External file</label>
			<div class="col-sm-6">{include file="widgets/select_box.tpl" _sel_id="fld_external_file" _sel_name="fld[external_file]" _sel_option=$ext_files _selected_val=$page.external_file}</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="fld_content">Contents</label>
			<div class="col-sm-9"><textarea id="fld_content" name="fld[content]">{$page.content}</textarea></div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="fld_active">Active</label>
			<div class="col-sm-6">{include file="widgets/select_box.tpl" _sel_id="fld_active" _sel_name="fld[active]" _sel_option=$yes_no _selected_val=$page.active}</div>
		</div>
		<div class="form-group"><label class="col-sm-2 control-label"><h4>SEO</h4></label></div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="fld_meta_keyword">Meta keyword(s)</label>
			<div class="col-sm-9"><input value="{$page.meta_keyword}" class="form-control" type="text" id="fld_meta_keyword" name="fld[meta_keyword]" placeholder="SEO: meta keywords" /></div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="fld_meta_description">Meta description</label>
			<div class="col-sm-6"><textarea id="fld_meta_description" name="fld[meta_description]" placeholder="SEO: Meta description" style="width:100%">{$page.meta_description}</textarea></div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="fld_change_freq">Change frequency</label>
			<div class="col-sm-6">{include file="widgets/select_box.tpl" _sel_id="fld_change_freq" _sel_name="fld[change_freq]" _sel_option=$change_freq _selected_val=$page.change_freq}</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="fld_priority">Page priority</label>
			<div class="col-sm-6" id="div_priority"><input id="fld_priority" type="text" name="fld[priority]" value="" /></div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="fld_indexme">Index me</label>
			<div class="col-sm-6">{include file="widgets/select_box.tpl" _sel_id="fld_indexme" _sel_name="fld[indexme]" _sel_option=$yes_no _selected_val=$page.indexme}</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="fld_followme">Follow links</label>
			<div class="col-sm-6">{include file="widgets/select_box.tpl" _sel_id="fld_followme" _sel_name="fld[followme]" _sel_option=$yes_no _selected_val=$page.followme}</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button id="submit" class="btn btn-primary" style="float:left;">Save</button>
				<div id="alert" class="alert alert-danger" role="alert" style="display:none;float:left;padding:5px;margin-bottom:0">Please fill the required fields (highlighted in red).</div>
			</div>
		</div>
	</form>
</div>
{capture name="JSSnippet"}{strip}
	<script type="text/javascript" src="/assets/js/plugins/codemirror/codemirror.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/codemirror/addon/edit/continuelist.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/codemirror/xml.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/codemirror/markdown.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/codemirror/formatting_cm-v2.36.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/summernote/summernote.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/summernote/summernote-ext-video.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/ion.rangeSlider.js"></script>
	<script type="text/javascript">

		var priority = {$priority};

		var url = (function() {
			{* Closures: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures *}

			function alphanum_only(str) {

				{*
					Replacing underscores with hyphen/dash (because we want it to follow Web::instance()->slug() (FatFreeFramework)
					-> See http://fatfreeframework.com/web#slug

					Replaces all non-alphanumeric characters with - (hyphen / dash)
					Replaces more than one contiguous hyphen with 1 hyphen
					Trims trailing hyphen
				*}

				var alphanum = '';
				var hyphen = '-';
				var double_hyphen = /--/g;

				if (str.trim() != '') {
					str = replace_accents(str);
					for (var j = 0; j < str.length; j++) {
						var aChar = str.charAt(j);

						var charCode = aChar.charCodeAt(0);
						if (!((charCode > 47 && charCode < 58) || (charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))) aChar = hyphen;

						alphanum = alphanum + aChar;
					}

					while (alphanum.match(double_hyphen)) alphanum = alphanum.replace(double_hyphen, hyphen);
					while (alphanum.charAt(0) == hyphen) alphanum = alphanum.substr(1, alphanum.length);
					while (alphanum.charAt(alphanum.length - 1) == hyphen) alphanum = alphanum.substr(0, alphanum.length - 1);

					alphanum = alphanum.toLowerCase();
				}

				return alphanum;
			}

			function replace_accents(str) {

				var replace_accents = '';

				if (str.trim() != '') {
					for (var j = 0; j < str.length; j++) {
						var aChar = str.charAt(j);
						var charCode = aChar.charCodeAt(0);

						if ((charCode > 191 && charCode < 198) || (charCode > 223 && charCode < 230)) aChar = 'a';
						if ((charCode == 198) || (charCode == 230)) aChar = 'ae';
						if ((charCode == 199) || (charCode == 231)) aChar = 'c';
						if ((charCode > 199 && charCode < 204) || (charCode > 231 && charCode < 235)) aChar = 'e';
						if ((charCode > 203 && charCode < 207) || (charCode > 235 && charCode < 240)) aChar = 'i';
						if ((charCode == 208) || (charCode == 240)) aChar = 'd';
						if ((charCode == 209) || (charCode == 241)) aChar = 'n';
						if ((charCode > 209 && charCode < 215) || (charCode > 241 && charCode < 247)) aChar = 'o';
						if ((charCode > 216 && charCode < 221) || (charCode > 248 && charCode < 253)) aChar = 'u';
						if ((charCode == 221) || (charCode == 253)) aChar = 'y';

						replace_accents = replace_accents + aChar;
					}
				}

				return replace_accents;
			}

			return {
				generate: function(str) {
					return alphanum_only(str);
				}
			};
		})();

		function generate_url() {
			var title = document.getElementById('fld_title').value;
			document.getElementById('fld_url').value = '/' + url.generate(title);
		}

		var markdown_editor = undefined;

		function toggle_editor(mode) {

			{* Markdown *}
			if (mode == 'M' || mode == 'T' || mode == 'E') {

				{* Destroy WYSIWYG editor *}
				$('#fld_content').destroy();

				{* Render only if not rendered yet - else it will create another instance of the Markdown editor *}
				if (markdown_editor == undefined) {

					markdown_editor = CodeMirror.fromTextArea(document.getElementById('fld_content'), {
						mode: 'markdown',
						lineNumbers: true,
						theme: 'default',
						extraKeys: { "Enter" : 'newlineAndIndentContinueMarkdownList' }
					});
				}
			}
			{* HTML WYSIWYG - Mode = H|S *}
			else {

				{* Destroy MARKDOWN editor, if initialised *}
				if (typeof markdown_editor == 'object') {
					markdown_editor.toTextArea();
					markdown_editor = undefined;
				}

				{* Will not create another instance of the Summernote WYSIWYG editor, if already created *}
				$('#fld_content').summernote({
					height: 250,
					tabsize: 4,
					codemirror: {
						theme: 'monokai'
					},
					toolbar: [
						['style', ['style']],
						['font', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
						['fontsize', ['fontsize']],
						['fontname', ['fontname']],
						['color', ['color']],
						['para', ['ul', 'ol', 'paragraph']],
						['height', ['height']],
						['insert', ['table', 'link', 'hr', 'picture', 'video']],
						['view', ['fullscreen', 'codeview', 'undo', 'redo']],
						['help', ['help']]
					]
				});
			}
		}

		function toggle_ext_file(mode) {

			if (mode == 'S' || mode == 'T') {
				$('#fg_ext_file').show();
			}
			else {
				$('#fg_ext_file').hide();
			}
		}

		$(document).ready(function() {

			{* Bind change event *}
			$('#fld_content_type').on('change', function(e) {
				toggle_editor($(this).val());
				toggle_ext_file($(this).val());
			});
			toggle_editor('{$page.content_type|default:'H'}');		{* Render editor *}
			toggle_ext_file('{$page.content_type|default:'H'}');	{* Show/hide external file selectbox *}

			{* See http://ionden.com/a/plugins/ion.rangeSlider/en.html *}
			$('#fld_priority').ionRangeSlider({
				min			: 0,
				max			: 10,
				step		: 1,
				prettify	: false,
				grid		: true,
				grid_snap	: true,
				hasGrid		: true,
				type		: 'single',
				from		: {$page.priority|default:5},
				onChange	: function(obj) {
					var p_index = (obj.fromNumber == '10' ? 'A' : obj.fromNumber);
					$('#div_priority').find('span.irs-single').html(priority[ p_index ]);
				}
			});

			$('#submit').on('click', function(e) {

				var mandatory_empty = false;

				$('#alert').hide();
				$('#fg_url').removeClass('has-error');
				$('#fg_title').removeClass('has-error');

				if ($.trim($('#fld_url').val()) == '') {
					e.preventDefault();
					$('#fg_url').addClass('has-error');
					mandatory_empty = true;
				}

				if ($.trim($('#fld_title').val()) == '') {
					e.preventDefault();
					$('#fg_title').addClass('has-error');
					mandatory_empty = true;
				}

				if (mandatory_empty) {
					$('#alert').show();
					return;
				}

				$('#frm').submit();
			});
		});
	</script>
{/strip}{/capture}
{append var='PostponedJS' value=$smarty.capture.JSSnippet scope='global'}
