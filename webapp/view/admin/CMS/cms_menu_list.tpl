<style>
	.c { text-align: center; }
	.w75 { width: 75px; }
	.sub_header th { background-color:#ffd; }
	.ACT { color:darkgreen; }
	.INACT { color:darkred; }
	input[type="text"] { width:50px; text-align: center; }
	.pad_left { padding-left:25px !important; }
</style>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="box box-solid box-primary">
			<div class="box-header">
				<h3 class="box-title">List of menus</h3>
				{if count($menus_by_lang) > 0}
					<div class="box-tools pull-right">
						<div class="btn-group">
							<button class="btn btn-info" type="button" onclick="save_columns('A')"><i class="fa fa-save"></i> Rank</button>
							<button class="btn btn-info" type="button" onclick="save_columns('B')"><i class="fa fa-save"></i> Active</button>
						</div>
					</div>
				{/if}
			</div>
			<div class="box-body">
				<table class="table table-bordered table-condensed table-hover">
					<thead>
					<tr style="white-space:nowrap">
						<th class="c w75">#</th>
						<th>Menu</th>
						<th>Opens</th>
						<th class="c w75">Rank</th>
						<th class="c w75">Active</th>
						<th class="w75"></th>
					</tr>
					</thead>
					<tbody id="List_of_Menus">
					{foreach $menus_by_lang as $lang => $menus}
						<tr class="sub_header"><th colspan="99">LANGUAGE: {$lang|strtoupper}</th></tr>
						{assign var="idx" value="0"}
						{foreach $menus as $v}
							{math equation="x + 1" x=$idx assign="idx"}
							<tr>
								<td class="c">{$idx}</td>
								<td>
									{if $v.menu_active eq 'Y'}<i class="fa fa-thumbs-o-up ACT"></i>{else}<i class="fa fa-thumbs-o-down INACT"></i>{/if}
									{$v.menu}
								</td>
								<td>
									{if $v.page_active eq 'Y' OR $v.page_id eq '#'}<i class="fa fa-thumbs-o-up ACT"></i>{else}<i class="fa fa-thumbs-o-down INACT"></i>{/if}
									{if $v.page_id eq '#'}#{else}{$v.title}{/if}
									<span class="pull-right">
										<a target="_blank" href="{$smarty.const.DYN_ADMIN}/CMS/pages/view/{$v.page_id}"><i class="fa fa-edit"></i></a>
										<a target="_blank" href="http://{$HOST}{$v.url}"{if $v.page_id eq '#'} style="visibility:hidden"{/if}><i class="fa fa-external-link"></i></a>
									</span>
								</td>
								<td class="c"><input type="text" id="fld_A_{$v.menu_id}" value="{$v.rank}" /></td>
								<td class="c">{include file="widgets/select_box.tpl"	_sel_option=$yes_no	_selected_val=$v.menu_active	_sel_id="fld_B_`$v.menu_id`"}</td>
								<td class="c"><a target="_blank" href="{$smarty.const.DYN_ADMIN}/CMS/menus/view/{$v.menu_id}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit</a></td>
							</tr>
							{if !empty($v.children)}
								{foreach $v.children as $c}
									<tr>
										<td></td>
										<td class="pad_left">
											{if $c.menu_active eq 'Y'}<i class="fa fa-thumbs-o-up ACT"></i>{else}<i class="fa fa-thumbs-o-down INACT"></i>{/if}
											{$c.menu}
										</td>
										<td class="pad_left">
											{if $c.page_active eq 'Y'}<i class="fa fa-thumbs-o-up ACT"></i>{else}<i class="fa fa-thumbs-o-down INACT"></i>{/if}
											{$c.title}
											<span class="pull-right">
												<a target="_blank" href="{$smarty.const.DYN_ADMIN}/CMS/pages/view/{$c.page_id}"><i class="fa fa-edit"></i></a>
												<a target="_blank" href="http://{$HOST}{$c.url}"><i class="fa fa-external-link"></i></a>
											</span>
										</td>
										<td class="c"><input type="text" id="fld_A_{$c.menu_id}" value="{$c.rank}" /></td>
										<td class="c">{include file="widgets/select_box.tpl"	_sel_option=$yes_no	_selected_val=$c.menu_active	_sel_id="fld_B_`$c.menu_id`"}</td>
										<td class="c"><a target="_blank" href="{$smarty.const.DYN_ADMIN}/CMS/menus/view/{$c.menu_id}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit</a></td>
									</tr>
								{/foreach}
							{/if}
						{/foreach}
					{foreachelse}
						<tr><td colspan="99" class="c">There are no menus for the moment. <a class="btn btn-success btn-xs" href="{$smarty.const.DYN_ADMIN}/CMS/menus/add"><i class="fa fa-plus"></i> Add a menu</a></td></tr>
					{/foreach}
					</tbody>
				</table>
			</div>
			<div style="display:none" class="working overlay"></div>
			<div style="display:none" class="working loading-img"></div>
		</div>
	</div>
</div>
{capture name="JSSnippet"}{strip}
<script type="text/javascript">

	function do_ajax(data_to_send) {

		if (typeof data_to_send != undefined) {

			$.ajax({
				url			: '{$smarty.const.DYN_ADMIN}/ajax/CMS_menus',
				type		: 'POST',
				dataType	: 'json',
				data		: data_to_send,
				beforeSend	: function(jqXHR, settings) { $('.working').show(); }
			})
					.done(function(res) { $('.working').hide(); })
					.fail(function(jqXHR, textStatus, errorThrown) { $('.working').hide(); });
		}
	}

	function save_columns(what) {

		if (typeof what == 'undefined')
			return;

		var data = [];
		var id, val;
		$.each($('#List_of_Menus').find('[id^="fld_' + what + '"]'), function(i, el) {

			id	= $(el).prop('id').substring(6);
			val	= $(el).val();

			{* Construct key-value pair, separated by | *}
			data.push(id + '|' + val);
		});

		do_ajax('col=' + what + '&key_vals=' + data.join('#'));
		return true;
	}
</script>
{/strip}{/capture}
{append var='PostponedJS' value=$smarty.capture.JSSnippet scope='global'}
