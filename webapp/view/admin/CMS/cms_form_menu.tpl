<div class="box box-primary">
	<div class="box-header"><h3 class="box-title"></h3></div>
	<form class="form-horizontal clearfix" method="POST" id="frm" action="{$smarty.const.DYN_ADMIN}/CMS/menus/save">
		<input type="hidden" name="fld[menu_id]" value="{$menu.menu_id}" />
		<div class="form-group" id="fg_menu">
			<label class="col-sm-2 control-label" for="fld_menu">Menu</label>
			<div class="col-sm-6"><input value="{$menu.menu}" class="form-control" type="text" id="fld_menu" name="fld[menu]" placeholder="Menu" /></div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="fld_parent_id">Parent menu</label>
			<div class="col-sm-9">{include file="widgets/select_box.tpl" _sel_id="fld_parent_id" _sel_name="fld[parent_id]" _sel_option=$menus _selected_val=$menu.parent_id}</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="fld_page_id">Opens this page</label>
			<div class="col-sm-9">{include file="widgets/select_box.tpl" _sel_id="fld_page_id" _sel_name="fld[page_id]" _sel_option=$pages _selected_val=$menu.page_id}</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="fld_active">Active</label>
			<div class="col-sm-6">{include file="widgets/select_box.tpl" _sel_id="fld_active" _sel_name="fld[active]" _sel_option=$yes_no _selected_val=$menu.active}</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="fld_meta_keyword">Rank</label>
			<div class="col-sm-1"><input value="{$menu.rank}" class="form-control" type="text" id="fld_rank" name="fld[rank]" placeholder="Rank" /></div>
		</div>
		{if is_array($langs)}
			<div class="form-group">
				<label class="col-sm-2 control-label" for="fld_lang">Language</label>
				<div class="col-sm-9">{include file="widgets/select_box.tpl" _sel_id="fld_lang" _sel_name="fld[lang]" _sel_option=$langs _selected_val=$menu.lang}</div>
			</div>
		{else}
			<input type="hidden" name="fld[lang]" value="{$langs}" />
		{/if}
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button id="submit" class="btn btn-primary" style="float:left;">Save</button>
				<div id="alert" class="alert alert-danger" role="alert" style="display:none;float:left;padding:5px;margin-bottom:0">Please fill the required fields (highlighted in red).</div>
			</div>
		</div>
	</form>
</div>
{capture name="JSSnippet"}{strip}
<script type="text/javascript">
$(document).ready(function() {
	$('#submit').on('click', function(e) {

		$('#alert').hide();
		$('#fg_menu').removeClass('has-error');

		if ($.trim($('#fld_menu').val()) == '') {
			e.preventDefault();
			$('#fg_menu').addClass('has-error');
			$('#alert').show();
			return;
		}

		$('#frm').submit();
	});
});
</script>
{/strip}{/capture}
{append var='PostponedJS' value=$smarty.capture.JSSnippet scope='global'}
