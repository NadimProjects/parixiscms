{assign var="_folder_" value="admin/CMS"}

{if $pg_type eq 'PagesList'}
	{include file="`$_folder_`/cms_page_list.tpl"}
{elseif $pg_type eq 'PagesAdd' OR $pg_type eq 'PagesView'}
	{include file="`$_folder_`/cms_form_page.tpl"}
{elseif $pg_type eq 'MenusList'}
	{include file="`$_folder_`/cms_menu_list.tpl"}
{elseif $pg_type eq 'MenusAdd' OR $pg_type eq 'MenusView'}
	{include file="`$_folder_`/cms_form_menu.tpl"}
{/if}

{assign var="_folder_" value=""}
