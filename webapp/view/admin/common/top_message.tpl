{if !empty($top_message)}
	{if $top_message.type eq 'success'}
		{assign var="tp_icon" value="check"}
	{elseif $top_message.type eq 'danger'}
		{assign var="tp_icon" value="ban"}
	{elseif $top_message.type eq 'warning'}
		{assign var="tp_icon" value="warning"}
	{else} {* info *}
		{assign var="tp_icon" value="info"}
	{/if}
	<div class="box-body" id="top_message">
		<div class="alert alert-dismissable alert-{$top_message.type}">
			<i class="fa fa-{$tp_icon}"></i>
			{if !$top_message.autoclose}<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>{/if}
			{if $top_message.title neq ''}<b>{$top_message.title}</b>{/if}&nbsp;
			{$top_message.message}
		</div>
	</div>
	{if $top_message.autoclose}
	{capture name="JSSnippet"}
		<script type="text/javascript"> $(document).ready(function() { $('#top_message').delay(3500).fadeOut(500); }); </script>
	{/capture}
	{append var='PostponedJS' value=$smarty.capture.JSSnippet scope='global'}
	{/if}
{/if}