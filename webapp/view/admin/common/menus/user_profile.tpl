<li class="treeview{if $main_menu eq 'User_Profile'} active{/if}">
	<a href="#">
		<i class="glyphicon glyphicon-user"></i>
		<span>{$AdminAuth.username|default:$AdminAuth.login}</span>
		<i class="fa fa-angle-left pull-right"></i>
	</a>
	<ul class="treeview-menu"{if $main_menu eq 'User_Profile'} style="display:block"{/if}>
		<li{if $URI eq "`$smarty.const.DYN_ADMIN`/profile"} class="active"{/if}><a href="{$smarty.const.DYN_ADMIN}/profile"><i class="fa fa-user"></i> My profile</a></li>
		<li><a href="{$smarty.const.DYN_ADMIN}/logout"><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
	</ul>
</li>