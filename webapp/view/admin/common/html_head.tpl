<meta charset="UTF-8">
<title>{$pg_title|default:''}{if !empty($pg_title)} - {/if}{$APP_DATA.app_name|default:'ParixisCMS'}</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<link href="/assets/css/vendors/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/vendors/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/vendors/ionicons.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />
{* HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries *}
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script>window.html5 || document.write('<script src="/assets/js/vendors/html5shiv.min.js"><\/script>')</script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<script>window.respond || document.write('<script src="/assets/js/vendors/respond.min.js"><\/script>')</script>
<![endif]-->
