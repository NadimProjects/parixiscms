# External scripts

When a CMS page is displayed, there may be situations where we have to display
other contents, in addition to the contents from the DB table. There may also
be situations where forms are posted, and processed.

This is the place to keep those scripts. **It is important to define any routes
in webapp/config/routes.php with the POST verb**

* Create a page (hence a URL/route) in the BackOffice
* Specify in BO that the page shall load an external script.
* Add a route with the POST verb on this URL, if form are to be posted for example
